/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Institute;

import java.util.ArrayList;

/**
 *
 * @author grant
 */
public class Institute {
    private String name;
    private ArrayList <Department> department = new ArrayList <Department>();

    public Institute(String name) {
        this.name = name;
    }

    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Department> getDepartment() {
        return department;
    }

    public void setDepartment(ArrayList<Department> department) {
        this.department = department;
    }
    
    public void addDepartment(Department department){
        this.department.add(department);
    }
    public void removeDepartment(Department department){
        this.department.remove(department);
    }
    
}
