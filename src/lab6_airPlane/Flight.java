/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab6_airPlane;

/**
 *
 * @author grant
 */
public class Flight {
    private Airline airline;
    private Airplane airplane;
    private int flightNum;
    private int date;
    private Airport current;
    private Airport destination;
    
 public Flight(Airline airline, Airplane airplane, int flightNum, int date, Airport current, Airport destination) {
        this.airline = airline;
        this.airplane = airplane;
        this.flightNum = flightNum;
        this.date = date;
        this.current = current;
        this.destination = destination;
    }

    
 
    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public int getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(int flightNum) {
        this.flightNum = flightNum;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public Airport getCurrent() {
        return current;
    }

    public void setCurrent(Airport current) {
        this.current = current;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }
   
}
