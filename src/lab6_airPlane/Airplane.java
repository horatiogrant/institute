/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab6_airPlane;

/**
 *
 * @author grant
 */
public class Airplane {
    private int idNum;
    private String model;
    private int numSeats;
    private String name;
    private Airline airline;

    public Airplane(int idNum, String model, int numSeats, String name, Airline airline) {
        this.idNum = idNum;
        this.model = model;
        this.numSeats = numSeats;
        this.name = name;
        this.airline = airline;
    }

   
    
    

    public int getIdNum() {
        return idNum;
    }

    public void setIdNum(int idNum) {
        this.idNum = idNum;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumSeats() {
        return numSeats;
    }

    public void setNumSeats(int numSeats) {
        this.numSeats = numSeats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }
    
    
}
